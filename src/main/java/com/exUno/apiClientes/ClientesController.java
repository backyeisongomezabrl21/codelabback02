package com.exUno.apiClientes;


import com.exUno.apiClientes.data.ClienteRepository;
import com.exUno.apiClientes.data.ClientesModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClientesController {

    @Autowired
    private ClienteRepository repository;

    /*Get para lista de clientes*/
    @GetMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<List<ClientesModel>> listaClientes(){
        List<ClientesModel> clientes = repository.findAll();
        return new ResponseEntity<List<ClientesModel>>(clientes, HttpStatus.OK);
    }

    /*Get para clientes por id*/
    @GetMapping(value = "/clientes/{id}", produces = "application/json")
    public ResponseEntity<ClientesModel> getCliente(@PathVariable String id){
        Optional<ClientesModel> cliente = repository.findById(id);
        return new ResponseEntity(cliente, HttpStatus.OK);
    }

    /*Post para registrar clientes */
    @PostMapping(value = "/clientes")
    public ResponseEntity<String> crearCliente(@RequestBody ClientesModel newCliente){
        ClientesModel addCliente = repository.insert(newCliente);
        return new ResponseEntity<>("Cliente creado correctamente", HttpStatus.CREATED);
    }

    /*Put para modificar clientes por id*/
    @PutMapping(value = "/clientes/{id}")
    public ResponseEntity<String> updateCliente(@RequestBody ClientesModel dataCliente, @PathVariable String id){
        Optional<ClientesModel> consulta = repository.findById(id);
        if (consulta.isPresent()){
            ClientesModel cliente = consulta.get();
            cliente.nombre = dataCliente.nombre;
            cliente.apellido = dataCliente.apellido;
            cliente.telefono = dataCliente.telefono;
            repository.save(cliente);

            return new ResponseEntity<>("cliente modificado correctamente", HttpStatus.OK);
        }
         return new ResponseEntity<>("El cliente no se encuentra registrado", HttpStatus.NOT_FOUND);
    }

    /*Delete de clientes por id*/
    @DeleteMapping("/clientes/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable String id){
        repository.deleteById(id);
        return new ResponseEntity<>("Cliente eliminado Correctamente", HttpStatus.OK);
    }


}
