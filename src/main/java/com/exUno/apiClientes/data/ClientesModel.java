package com.exUno.apiClientes.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("clientesYeison")
public class ClientesModel {

    public String id;
    public String nombre;
    public String apellido;
    public String telefono;


    public ClientesModel(String  id, String nombre, String apellido, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
    }

}
