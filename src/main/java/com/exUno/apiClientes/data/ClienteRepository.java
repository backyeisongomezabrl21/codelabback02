package com.exUno.apiClientes.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ClienteRepository extends MongoRepository<ClientesModel, String> {


}
